import { Component, OnInit } from '@angular/core';

class Livros {
  titulo: string;
  autor: string;
  preco: number;
  codigo_livro: string;
  genero: string;
  numero_de_paginas: number;
  capa: string;
}

@Component({
  selector: 'app-anuncios',
  templateUrl: './anuncios.page.html',
  styleUrls: ['./anuncios.page.scss'],
})
export class AnunciosPage implements OnInit {

  livros: Livros[];

  constructor() { }

  ngOnInit() {
    this.livros = [
      {
        titulo: 'A sociedade do anel',
        autor: 'J.R.R. Tolkien',
        preco: 100,
        codigo_livro: '83D7FAG0ES',
        genero: 'Aventura',
        numero_de_paginas: 500,
        capa: '../assets/images/a_sociedade_do_anel.jpg'
      },
      {
        titulo: 'A Volta ao mundo em 80 dias',
        autor: 'Júlio Verne',
        preco: 50,
        codigo_livro: 'D9FS02IA7A',
        genero: 'Aventura',
        numero_de_paginas: 200,
        capa: '../assets/images/a_volta_ao_mundo_em_80_dias.jpg'
      },
      {
        titulo: 'O Alquimista',
        autor: 'Paulo Coelho',
        preco: 60,
        codigo_livro: 'G10V4NNARA',
        genero: 'Romance Alegórico',
        numero_de_paginas: 250,
        capa: '../assets/images/o_alquimista.jpg'
      }
    ]
  }

}
